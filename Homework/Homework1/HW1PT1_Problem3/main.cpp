/*
* Name:Kevin Rivas
* Student ID:2437797
* Date:February 27, 2014
* HW:1
* Problem:3
* I certify this is my own work and code
*/

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    //declare variables
    int number_of_pods, peas_per_pod, total_peas;
    
    //Edit for Problem 2
    cout << "Hello" << endl;
    
    cout << "Press return after entering a number." << endl;
    cout << "Enter the number of pods:"<< endl;
    
    cin>> number_of_pods;
    
    cout << "Enter the number of peas in a pod: " << endl;
    cin>> peas_per_pod;
    
    //changed * to / according to problem 3
    total_peas = number_of_pods / peas_per_pod;
    cout << "If you have ";
    cout << "pea pods" << endl;
    cout << "and " ;
    cout << peas_per_pod;
    cout << " peas in each pod, then " << endl;
    cout << "you have " ;
    cout << total_peas;
    cout << " peas in all the pods. " << endl;
    
    //Edit for Problem 2
    cout << "Good-bye" << endl;
    return 0;
}

