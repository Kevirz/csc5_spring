/* 
 ** Name:Kevin_Rivas
 * Student ID:2437797
 * Date:Febuary 26, 2013
 * HW:Part 1
 * Problem:5
 * I certify this is my own work and code

 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
   
   int num1, num2, sum, product;
   
   cout << "Input two integers" << endl;
   cin >> num1 >> num2 ;
   
   sum = num1 + num2;
   product = num1 * num2;
   
   cout<<"The sum of " << num1 << " and " << num2 << " is " << sum << endl;
   cout<<"The product " << num1 << " and " << num2 << " is " << product << endl;
   
   
    return 0;
}

