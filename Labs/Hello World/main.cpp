/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 20, 2014, 10:42 AM
 */

#include <cstdlib>//cstdlib library not needed
#include <iostream>// iostream

//Gives
using namespace std;

/*
 * 
 */


//There is always and only one main
//Programs always start at main
int main(int argc, char** argv) {
    //cout specifies output
    //endl creates a new line
    //<< specifies stream operator
    //All statements end in a semicolon
 
    //Using a programmer-defined identifier/variable
    //message is a variable
    //string is a data tye
    //=is an assignment operator
    //Assign right to left
    //string message ="Hello World";
    string message; //Variable declaration
    message= "Kevin Rivas"; //Variable initialization
    cout<< message << endl;
    
    //C++ ignored whitespace
    cout<< "Kevin Rivas";
    
           
    
    
    //If program hits return, it ran sucessfully
    return 0;
    //All my code is within curly brace
    
    
}

