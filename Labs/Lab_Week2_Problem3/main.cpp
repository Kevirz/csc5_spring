/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 4, 2014, 12:18 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    int test1,test2,test3;
    
    cout << "Enter First test score:";
    
    cin >> test1;
    
    cout <<"Enter Second test score:";
    
    cin >> test2;
    
    cout << "Enter Third test score:";
    
    cin >> test3;
    
    cout<< "Your average is " << (test1+test2+test3)/3 << "."<< endl;
    
    return 0;
}

